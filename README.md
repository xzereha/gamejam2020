Game made for the Global Game Jam 2020 by
Oliver Öhrström,
Jonas Ekblad,
Hugo Hansen,
Fredrik Sy.

This project is Licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License.
https://creativecommons.org/licenses/by-nc-sa/4.0/
