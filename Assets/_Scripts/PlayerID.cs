﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerID : MonoBehaviour
{
    public static readonly Color Blue = new Color(0f, 0f, 0.4f);
    public static readonly Color Red = new Color(0.5f, 0f, 0f);
    public static readonly Color Yellow = new Color(0.8f, 0.8f, 0f);
    public static readonly Color Green = new Color(0f, 0.5f, 0f);

    public static int USED_ID = 0;

    public int Id { get; private set; }
    public string Name { get; private set; }

    private void Awake()
    {
        Id = ++USED_ID;

        switch (Id)
        {
            case 1:
                GetComponent<SpriteRenderer>().color = Blue;
                transform.position = new Vector3(-3.5f, 0, 0);
                Name = "Blue";
                break;
            case 2:
                GetComponent<SpriteRenderer>().color = Red;
                transform.position = new Vector3(-2, 0, 0);
                Name = "Red";
                break;
            case 3:
                GetComponent<SpriteRenderer>().color = Yellow;
                transform.position = new Vector3(2, 0, 0);
                Name = "Yellow";
                break;
            case 4:
                GetComponent<SpriteRenderer>().color = Green;
                transform.position = new Vector3(3.5f, 0, 0);
                Name = "Green";
                break;
            default:
                transform.position = new Vector3(0, 2, 0);
                GetComponent<SpriteRenderer>().color = Color.black;
                Name = "Black";
                break;
        }
    }
}
