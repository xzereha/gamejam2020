﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerSpawner : MonoBehaviour
{
    public static bool Keyboard = true;
    public static int Controllers = 3;

    [SerializeField] private PlayerInputManager m_playerInput;

    private void Awake()
    {
        var index = 0;

        if (Keyboard)
        {
            m_playerInput.JoinPlayer(index++, -1, "Keyboard");
        }

        for(int i = 0; i < Controllers && index < 4; ++i)
        {
            m_playerInput.JoinPlayer(index++, -1, "Gamepad");
        }
    }
}
