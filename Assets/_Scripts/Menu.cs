﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [SerializeField] private Image m_keyboardImage;
    [SerializeField] private TMPro.TMP_Text m_playerText;

    private bool m_keyboard = true;
    private int m_playerAmount = 2;

    private void Awake()
    {
        m_keyboard = PlayerSpawner.Keyboard;
        m_playerAmount = m_keyboard ? PlayerSpawner.Controllers + 1 : PlayerSpawner.Controllers;
        m_keyboardImage.color = m_keyboard ? Color.green : Color.red;
        m_playerText.text = m_playerAmount.ToString();
    }

    public void Increase()
    {
        m_playerAmount = Mathf.Clamp(m_playerAmount + 1, 2, 4);
        m_playerText.text = m_playerAmount.ToString();
    }

    public void Decrease()
    {
        m_playerAmount = Mathf.Clamp(m_playerAmount - 1, 2, 4);
        m_playerText.text = m_playerAmount.ToString();
    }

    public void ToggleKeyboard()
    {
        m_keyboard = !m_keyboard;
        m_keyboardImage.color = m_keyboard ? Color.green : Color.red;
    }

    public void Go()
    {
        PlayerSpawner.Keyboard = m_keyboard;
        PlayerSpawner.Controllers = m_keyboard ? m_playerAmount - 1 : m_playerAmount;
        SceneManager.LoadScene(1);
        PlayerID.USED_ID = 0;
    }

    public void Stop()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif

    }
}
