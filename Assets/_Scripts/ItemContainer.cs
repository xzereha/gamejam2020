﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Item
{
    public Sprite Sprite;
    public string Name;

    public static bool operator ==(Item lhs, Item rhs)
    {
        if(lhs.Name == rhs.Name)
        {
            return true;
        }
        return false;
    }

    public static bool operator !=(Item lhs, Item rhs)
    {
        return lhs == rhs;
    }

    public override int GetHashCode()
    {
        if(Name == null || Name.Length == 0)
        {
            return 0;
        }
        return Name.GetHashCode();
    }
}

[CreateAssetMenu()]
public class ItemContainer : ScriptableObject
{
    public Item[] Items;
}
