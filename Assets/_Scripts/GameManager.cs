﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public ItemContainer ItemContainer;
    public Goal Goal;
    public int[] Scores = { -1, -1, -1, -1 };

    [SerializeField] private float m_itemChangeDelay = 5;
    [SerializeField] private BoundsInt m_bounds;
    [SerializeField] private GameObject m_worldItemPrefab;
    private GameObject m_currentWorldItem;
    private Item m_currentItem;
    private Dictionary<Item, GameObject> m_loadedItems;
    private bool m_respawning = false;

    private void Awake()
    {
        m_loadedItems = new Dictionary<Item, GameObject>();
    }

    private void Start()
    {
        foreach(var item in ItemContainer.Items)
        {
            SpawnItem(item);
        }

        foreach(var player in GameObject.FindObjectsOfType<PlayerID>())
        {
            var id = player.Id - 1;
            if(id < Scores.Length)
            {
                Scores[id] = 0;
            }
        }

        m_respawning = true;
        StartCoroutine(NewRequiredItem());
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }

        if(m_currentWorldItem == null && !m_respawning)
        {
            SpawnItem(m_currentItem);
            m_respawning = true;
            StartCoroutine(NewRequiredItem());
        }
    }

    private IEnumerator NewRequiredItem()
    {
        yield return new WaitForSeconds(m_itemChangeDelay);

        m_currentItem = ItemContainer.Items[Random.Range(0, ItemContainer.Items.Length)];
        Goal.RequiredItem = m_currentItem;

        m_currentWorldItem = m_loadedItems[m_currentItem];
        m_respawning = false;
    }

    private void SpawnItem(Item item)
    {
        var go = Instantiate(m_worldItemPrefab);
        go.GetComponent<WorldItem>().Item = item;
        m_loadedItems[item] = go;

        do
        {
            var x = Random.Range(m_bounds.min.x, m_bounds.max.x);
            var y = Random.Range(m_bounds.min.y, m_bounds.max.y);
            if(Physics2D.OverlapPoint(new Vector2(x, y)) == false)
            {
                go.transform.position = new Vector3(x + 0.5f, y + 0.5f, 0);
                break;
            }
        } while (true);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(m_bounds.center, m_bounds.size);
    }
}
