﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer), typeof(Rigidbody2D), typeof(Collider2D))]
public class WorldItem : MonoBehaviour
{
    public Item Item
    {
        get { return m_item; }
        set { SetItem(value); }
    }

    private Item m_item;

    private void SetItem(Item item)
    {
        m_item = item;
        GetComponent<SpriteRenderer>().sprite = m_item.Sprite;
    }
}
