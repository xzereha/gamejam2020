﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody2D), typeof(PlayerInput), typeof(Collider2D))]
public class PlayerController : MonoBehaviour
{
    public Vector2 MoveDirection { get; private set; }
    public Vector2 FacingDirection { get; private set; }
    public Vector2 AimDirection { get; private set; }
    public bool Stunned { get; set; }
    public bool Dashed { get; private set; }

    [SerializeField] private float m_speed = 4;
    [SerializeField] private float m_throwingForce = 10;
    [SerializeField] private float m_throwingOffset = 0.5f;
    [SerializeField] private float m_stunDuration = 2;
    [SerializeField] private float m_stunVelocity = 6;
    [SerializeField] private float m_dashForce = 5;
    [SerializeField] private float m_dashCooldown = 3;
    [SerializeField] private AudioClip[] m_hitSounds;
    [SerializeField] private AudioClip[] m_runSounds;
    [SerializeField] private AudioClip[] m_pickUpSounds;

    private float m_dashElapsed = 0;
    

    private Rigidbody2D m_rigidbody;
    public WorldItem CarriedItem { get; set; }
    private Collider2D[] m_hits;

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_hits = new Collider2D[28];
    }

    private void FixedUpdate()
    {
        if (!Stunned)
        {
            m_rigidbody.position += MoveDirection * m_speed * Time.deltaTime;
        }

        m_dashElapsed += Time.fixedDeltaTime;
        if (m_dashElapsed >= 0.1)
        {
            if (Dashed)
            {
                Dashed = false;
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            }
        }
    }

    private void Pickup()
    {
        var hits = Physics2D.OverlapCircleNonAlloc(transform.position + new Vector3(FacingDirection.x, FacingDirection.y, 0), 0.5f, m_hits);
        for(int i = 0; i < hits; ++i)
        {
            if (m_hits[i].gameObject.TryGetComponent<WorldItem>(out WorldItem item))
            {
                CarriedItem = item;
                CarriedItem.transform.SetParent(transform);
                CarriedItem.GetComponent<Collider2D>().enabled = false;
                CarriedItem.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                CarriedItem.GetComponent<SpriteRenderer>().sortingLayerName = "Held Items";
                CarriedItem.transform.localPosition = new Vector3(0, 0.2f, 0);
                GetComponent<AudioSource>().PlayOneShot(m_pickUpSounds[Random.Range(0, m_pickUpSounds.Length)], 0.5f);
                return;
            }
        }
    }

    private void Throw()
    {
        if(CarriedItem != null)
        {
            CarriedItem.transform.SetParent(null);
            CarriedItem.transform.position = transform.position + new Vector3(AimDirection.x, AimDirection.y, 0) * m_throwingOffset;
            CarriedItem.GetComponent<Collider2D>().enabled = true;
            CarriedItem.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            CarriedItem.GetComponent<Rigidbody2D>().AddForce(AimDirection * m_throwingForce, ForceMode2D.Impulse);
            CarriedItem.GetComponent<SpriteRenderer>().sortingLayerName = "Ground Items";
            CarriedItem = null;
        }
    }

    private void Dash()
    {
        if (MoveDirection.magnitude != 0)
        {
            m_rigidbody.AddForce(MoveDirection * m_dashForce);
            Dashed = true;
            m_dashElapsed = 0;
            GetComponent<ParticleSystem>().Play();
            GetComponent<AudioSource>().PlayOneShot(m_runSounds[Random.Range(0, m_runSounds.Length)], 0.25f);
        }
    }

    private void DropItem()
    {
        if (CarriedItem != null)
        {
            CarriedItem.transform.SetParent(null);
            CarriedItem.transform.position = transform.position;
            CarriedItem.GetComponent<Collider2D>().enabled = true;
            CarriedItem.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            CarriedItem.GetComponent<SpriteRenderer>().sortingLayerName = "Ground Items";
            CarriedItem = null;
        }
    }

    private IEnumerator Stun(float duration)
    {
        Stunned = true;
        yield return new WaitForSeconds(duration);
        Stunned = false;
    }

    public void OnMovement(InputAction.CallbackContext ctx)
    {
        MoveDirection = ctx.ReadValue<Vector2>();
        if(MoveDirection.sqrMagnitude > 0)
        {
            FacingDirection = MoveDirection;
        }
    }

    public void OnAim(InputAction.CallbackContext ctx)
    {
        AimDirection = ctx.ReadValue<Vector2>();
    }

    public void OnPickup(InputAction.CallbackContext ctx)
    {
        if (ctx.started && !Stunned)
        {
            Pickup();
        }
        else if(ctx.canceled && !Stunned)
        {
            Throw();
        }
    }

    public void OnDash(InputAction.CallbackContext ctx)
    {
        if (ctx.started && !Stunned && m_dashElapsed >= m_dashCooldown)
        {
            Dash();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.TryGetComponent<Rigidbody2D>(out Rigidbody2D rigidbody))
        {
            if (collision.gameObject.TryGetComponent<WorldItem>(out _))
            {
                if(rigidbody.velocity.sqrMagnitude >= m_stunVelocity * m_stunVelocity)
                {
                    DropItem();
                    FacingDirection = rigidbody.velocity.x < 0 ? Vector2.right : Vector2.left;
                    if (Stunned == false)
                    {
                        GetComponent<AudioSource>().PlayOneShot(m_hitSounds[Random.Range(0, m_hitSounds.Length)], 3f);
                        //GetComponent<Rigidbody2D>().AddForce(rigidbody.velocity);
                        StartCoroutine(Stun(m_stunDuration));
                    }
                }
            }
        }
    }
}
