﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(PlayerController))]
public class PlayerAnimatorController : MonoBehaviour
{
    public static int HashAimDirectionX = Animator.StringToHash("AimDirectionX");
    public static int HashMoveDirectionX = Animator.StringToHash("MoveDirectionX");
    public static int HashMoveDirectionY = Animator.StringToHash("MoveDirectionY");
    public static int HashFacingDirectionX = Animator.StringToHash("FacingDirectionX");
    public static int HashIsCarryingItem = Animator.StringToHash("IsCarryingItem");
    public static int HashDance = Animator.StringToHash("Dance");
    public static int HashStunned = Animator.StringToHash("Stunned");
    public static int HashStunActive = Animator.StringToHash("StunActive");

    private Animator m_animator;
    private PlayerController m_playerController;

    private void Start()
    {
        m_animator = GetComponent<Animator>();
        m_playerController = GetComponent<PlayerController>();
    }

    private void Update()
    {
        if (m_animator == null || m_playerController == null)
            return;

        float aimDirectionX = Mathf.Round(m_playerController.AimDirection.x);
        Vector2 moveDirection = m_playerController.MoveDirection;
        Vector2 facingDirection = m_playerController.FacingDirection;

        if (aimDirectionX == -1 || aimDirectionX == 1)
            m_animator.SetFloat(HashAimDirectionX, aimDirectionX);
        else if (facingDirection.x != 0)
            m_animator.SetFloat(HashAimDirectionX, facingDirection.x);

        m_animator.SetBool(HashStunned, m_playerController.Stunned);
        m_animator.SetBool(HashIsCarryingItem, m_playerController.CarriedItem != null);
        m_animator.SetFloat(HashMoveDirectionX, moveDirection.x);
        m_animator.SetFloat(HashMoveDirectionY, moveDirection.y);
        m_animator.SetFloat(HashFacingDirectionX, facingDirection.x);

        if (!m_playerController.Stunned)
            m_animator.SetBool(HashStunActive, false);
    }

    public void Dance()
    {
        m_animator.SetTrigger(HashDance);
    }
}
