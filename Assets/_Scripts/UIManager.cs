﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameManager m_gameManager;
    [SerializeField] private Goal m_goal;
    [SerializeField] private Image m_uiImage;
    [SerializeField] private TMPro.TMP_Text[] m_scores;
    private void Update()
    {
        var item = m_goal.RequiredItem;
        if(item.Name != null && item.Name.Length > 0)
        {
            m_uiImage.enabled = true;
            m_uiImage.sprite = item.Sprite;
        }
        else
        {
            m_uiImage.enabled = false;
        }

        for(int i = 0; i < m_scores.Length && i < m_gameManager.Scores.Length; ++i)
        {
            var score = m_gameManager.Scores[i];
            if(score >= 0)
            {
                //m_scores[i].text = "Player " + (i + 1) + ": " + score;
                switch (i)
                {
                    case 0:
                        m_scores[i].text = "BLUE: " + score;
                        break;
                    case 1:
                        m_scores[i].text = "RED: " + score;
                        break;
                    case 2:
                        m_scores[i].text = "YELLOW: " + score;
                        break;
                    case 3:
                        m_scores[i].text = "GREEN: " + score;
                        break;
                }
                m_scores[i].enabled = true;
            }
            else
            {
                m_scores[i].enabled = false;

            }
        }
    }
}
