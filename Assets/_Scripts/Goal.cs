﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    [HideInInspector] public Item RequiredItem;
    [SerializeField] private GameManager m_gameManager;
    [SerializeField] public int GoalScore = 5;
    [SerializeField] private int m_victoryScreenDelay = 10;
    [SerializeField] private TMP_Text m_victoryText;
    [SerializeField] private AudioClip[] m_pointSounds;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.TryGetComponent<PlayerController>(out PlayerController player))
        {
            if (col.TryGetComponent<PlayerID>(out PlayerID playerID))
            {
                var item = player.CarriedItem;
                if (item != null && item.Item == RequiredItem)
                {
                    var id = playerID.Id - 1;
                    if (id < m_gameManager.Scores.Length)
                    {
                        m_gameManager.Scores[id]++;
                        if (m_gameManager.Scores[id] >= GoalScore)
                        {
                            StartCoroutine(VictoryScreenRoutine(id + 1));
                        }
                    }
                    player.CarriedItem = null;
                    RequiredItem.Name = null;
                    Destroy(item.gameObject);
                    GetComponent<AudioSource>().PlayOneShot(m_pointSounds[Random.Range(0, m_pointSounds.Length)], 0.5f);
                }
            }
        }
    }

    private IEnumerator VictoryScreenRoutine(int playerIDWon)
    {
        foreach (PlayerID playerID in FindObjectsOfType<PlayerID>())
        {
            if (playerIDWon != playerID.Id)
            {
                if (playerID.TryGetComponent(out PlayerController player))
                {
                    player.Stunned = true;
                }
            }
            else
            {
                playerID.GetComponent<PlayerAnimatorController>().Dance();
                m_victoryText.text = "MR. " + playerID.Name + " Won!";
                m_victoryText.gameObject.SetActive(true);
            }
        }

        yield return new WaitForSeconds(m_victoryScreenDelay);
        SceneManager.LoadScene(0);
    }
}
