﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UFOScript : MonoBehaviour
{
    private const int MaxSprites = 5;
    
    [SerializeField] private GameManager m_gameManager;
    [SerializeField] private Goal m_goal;
    [SerializeField] private Sprite[] m_sprites;
    private SpriteRenderer m_spriteRenderer;

    private void Start()
    {
        m_spriteRenderer = GetComponent<SpriteRenderer>();
    }
    void Update()
    {
        int index = m_gameManager.Scores.Max() * (MaxSprites / m_goal.GoalScore);
        m_spriteRenderer.sprite = m_sprites[Mathf.Min(index, 4)];
    }
}
