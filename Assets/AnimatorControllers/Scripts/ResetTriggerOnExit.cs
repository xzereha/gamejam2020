﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetTriggerOnExit : StateMachineBehaviour
{
    [SerializeField] private string m_name;
    private int m_id;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (m_id == 0)
            m_id = Animator.StringToHash(m_name);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(m_id, false);
    }
}
