﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetBoolOnEnter : StateMachineBehaviour
{
    [SerializeField] private string m_name;
    [SerializeField] private bool m_value;
    private int m_id;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (m_id == 0)
            m_id = Animator.StringToHash(m_name);

        animator.SetBool(m_id, m_value);
    }
}
